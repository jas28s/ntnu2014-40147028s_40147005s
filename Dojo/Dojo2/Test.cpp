#include "stdafx.h"
#include "gtest/gtest.h"
#include <string>
#include "Game.h"
using namespace std;

class MyTest : public ::testing::Test
{
};

 
TEST_F(MyTest, AllZero)
{ 
	Game g;
	for (int i = 0; i < 20; i++)
	{
		g.Roll(0); 
	}
	
	EXPECT_EQ(0 * 20, g.GetScore());
}

TEST_F(MyTest, AllOne)
{
	Game g;
	for (int i = 0; i < 20; i++)
	{
		g.Roll(1);
	}
	EXPECT_EQ(1 * 20, g.GetScore());
}

TEST_F(MyTest, KnowScoreSize){
  Game g;
  g.Roll(10);
  EXPECT_EQ(2,g.GetScoreSize());
  for(int i=1 ; i< 9 ;i++){
	  g.Roll(10);
  }
  EXPECT_EQ(18,g.GetScoreSize());
  for(int i=0;i<3;i++){
	  g.Roll(10);
  }
  EXPECT_EQ(21,g.GetScoreSize());
}

/*TEST_F(MyTest, WithoutLastStrike)
{
	Game g;
	// frame 1~9
	for (int i = 0; i < 9; i++)
	{
		g.Roll(10);
	}
		

	EXPECT_EQ(270, g.GetScore());
}*/
TEST_F(MyTest,SizeOfMTIMES){
	int arr[21];
	EXPECT_EQ(21*4,sizeof(arr));
}
TEST_F(MyTest, AllStrike)
{
	Game g;
	// frame 1~9
	for (int i = 0; i < 9; i++)
	{
		g.Roll(10);
	}

	// frame 10
	for (int i = 0; i < 3; i++)
	{
		g.Roll(10);
	}

	EXPECT_EQ(300, g.GetScore());
}

