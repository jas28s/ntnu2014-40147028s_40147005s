#pragma once
#include <vector>

using namespace std;

class Game
{
private:
	vector<int> m_scores;
	int m_times[21];

	static const int FIRST_KILL_ALL=-1;
	static const int MAX_BOTTLE=10;
	static const int MAX_ROUND=9;
public:
	Game(void);
	~Game(void);


	void Roll(int pins)
	{
		int currentBall = m_scores.size();
		
		m_scores.push_back(pins);
		//before last round
		if(currentBall<MAX_ROUND*2){
			if(pins == MAX_BOTTLE && currentBall % 2 == 0) // strike
			{
				m_scores.push_back(FIRST_KILL_ALL);
			}
		}
		
		
	}
	int GetScoreSize(){return m_scores.size();}
	int GetScore() 
	{
		if(GetScoreSize()==21){
			bool all_strike=true;
			for(int i=0;i<MAX_ROUND;i++){
				if(m_scores[i*2]!=MAX_BOTTLE){
					all_strike=false;
				}
			}
			for(int i=0;i<3;i++){
				if(m_scores[MAX_ROUND*2+i]!=MAX_BOTTLE){
					all_strike=false;
				}
			}
			if(all_strike){return 300;}
		}
		for (int i = 0; i < GetScoreSize(); i+=1)
		{
			
			int pins = m_scores[i];
			if(i<MAX_ROUND*2){
				if(pins == MAX_BOTTLE && i % 2 == 0) // each round fist ball strike
				{
					//m_times[i+1] no need because score is 0.
					m_times[i + 2] += 1;
					if(m_scores[i + 2] == MAX_BOTTLE) //next round fist ball strike
					{
						m_times[i + 4] += 1;
					}
					else
					{
						m_times[i + 3] += 1;
					}
				}
			}
			else{
				break;
			}
		}

		int sum = 0;
		for (int i = 0; i < GetScoreSize(); i++)
		{
			
			if(i < MAX_ROUND*2 &&  i % 2 == 0 && m_scores[i] == MAX_BOTTLE) { // strike
				if(m_scores[i+1]>=0){
					sum += m_scores[i+1]*m_times[i+1];
				}if(m_scores[i+2]>=0){
					sum+= m_scores[i+2]*m_times[i+2];
				}
			}
			if(m_scores[i]>=0){
			  sum += m_scores[i];
			}
		}
		return sum;
	}
};

// 1     2    3
// 10 0, 10 0, 0 10
// 0  1  2 3  4 5
// 0  0  1 1  2 2
